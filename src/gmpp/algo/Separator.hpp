#ifndef HEADER_gmpp_algo_Separator_hpp_ALREADY_INCLUDED
#define HEADER_gmpp_algo_Separator_hpp_ALREADY_INCLUDED

#include "gubg/mss.hpp"
#include "gubg/Range.hpp"
#include <boost/graph/properties.hpp>
#include <vector>
#include <limits>
#include <stack>
#include <set>
#include <map>

namespace gmpp { namespace algo { 

    struct Separator
    {
        using vertex_index_type = std::size_t;
        using color_type        = std::size_t;
        using VertexColorMap    = std::vector<color_type>;
        using Component         = std::set<std::size_t>;

        struct Entry
        {
            vertex_index_type index;
            std::vector<bool> neighbour_components;
        };

        static constexpr color_type separator_color() { return std::numeric_limits<color_type>::max(); }
        static constexpr color_type unseen_color()    { return std::numeric_limits<color_type>::max() - 1; }

        Separator()
        {
        }

        template <typename VertexIterator, typename UndirectedGraph>
        Separator(gubg::Range<VertexIterator> separator_range, const UndirectedGraph & graph)
            : Separator(separator_range, get(boost::vertex_index, graph), graph)
        {
        }

        template <typename VertexIterator, typename UndirectedGraph, typename VertexIDMap>
        Separator(gubg::Range<VertexIterator> separator_range, VertexIDMap vertex_id_map, const UndirectedGraph & graph)
            : vertex_color_map_(num_vertices(graph), unseen_color())
        {
            // fill in the separator
            auto map = make_separator_map_(separator_range, vertex_id_map);
            separator_.reserve(map.size());
            for(const auto & p : map)
            {
                vertex_index_type ix = p.first;
                {
                    Entry e;
                    e.index = ix;
                    separator_.push_back(std::move(e));
                }
                vertex_color_map_[ix] = separator_color();
            }
        
            // classify the vertices in components
            color_type num_components = 0;
            auto range = gubg::make_range(vertices(graph));
            while(advance_to_unseen_(range, vertex_id_map))
            {
                auto v = range.front();
                fill_from_seed_(v, num_components++, vertex_id_map, graph);
            }
            
            // fill in the components
            components_.resize(num_components, {});
            for(auto & c : separator_)
                c.neighbour_components.resize(num_components, false);

            fill_in_components_(map, vertex_id_map, graph);
        }

        color_type get_color(vertex_index_type ix) const
        {
            return vertex_color_map_[ix];
        }

        const Component & component(color_type color) const
        {
            return components_[color];
        }

        color_type num_components() const
        {
            return components_.size();
        }

        private:
        template <typename VertexIDMap, typename VertexIterator>
        bool advance_to_unseen_(gubg::Range<VertexIterator> & vertices, VertexIDMap vertex_id_map)
        {
            while(!vertices.empty())
            {
                auto ix = get(vertex_id_map, vertices.front());
                if (vertex_color_map_[ix] == unseen_color())
                    return true;

                vertices.pop_front();
            }

            return false;
        }
        
        template <typename Vertex, typename VertexIDMap, typename Graph>
        void fill_from_seed_(Vertex seed, color_type color, VertexIDMap vertex_id_map, const Graph & graph)
        {
            std::stack<Vertex> todo;
            todo.push(seed);

            while(!todo.empty())
            {
                auto v = todo.top();
                todo.pop();

                auto ix = get(vertex_id_map, v);
                if (vertex_color_map_[ix] != unseen_color())
                    continue;

                vertex_color_map_[ix] = color;
                for(auto u : gubg::make_range(adjacent_vertices(v, graph)))
                    todo.push(u);
            }
        }

        template <typename VertexIterator, typename VertexIDMap>
        std::map<vertex_index_type, typename std::iterator_traits<VertexIterator>::value_type>
        make_separator_map_(const gubg::Range<VertexIterator> & separator, VertexIDMap vertex_id_map)
        {
            std::map<vertex_index_type, typename std::iterator_traits<VertexIterator>::value_type> map;
            for(const auto & v : separator)
                map[get(vertex_id_map, v)] = v;
            return map;

        }
        template <typename SeparatorMap, typename VertexIDMap, typename Graph>
        void fill_in_components_(const SeparatorMap & separator, VertexIDMap vertex_id_map, const Graph & g)
        {
            std::size_t sep_index = 0;
            auto it = separator.begin();
            auto add_to_end = [](auto & set, auto v) { set.insert(set.end(), v); };

            for(vertex_index_type ix = 0; ix < vertex_color_map_.size(); ++ix)
            {
                auto component = vertex_color_map_[ix];
                if (component == separator_color())
                {
                    assert(sep_index < separator.size());
                    assert(it != separator.end());
                    auto & entry = separator_[sep_index];
                    assert(entry.index == ix);
                    assert(it->first == ix);

                    // loop over the adjacent vertices
                    for(const auto & u : gubg::make_range(adjacent_vertices(it->second, g)))
                    {
                        vertex_index_type u_ix = get(vertex_id_map, u);
                        color_type u_comp = vertex_color_map_[u_ix];

                        if(u_comp != separator_color())
                        {
                            add_to_end(components_[u_comp], ix);
                            entry.neighbour_components[u_comp] = true;
                        }

                    }

                    ++sep_index;
                    ++it;
                }
                else
                {
                    add_to_end(components_[component], ix);
                }
            }
        }
        
        std::vector<Entry> separator_;
        VertexColorMap vertex_color_map_;
        std::vector<Component> components_;
    };

} }

#endif

