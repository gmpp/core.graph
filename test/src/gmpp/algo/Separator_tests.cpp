#include "gmpp/algo/Separator.hpp"
#include "catch.hpp"
#include <boost/graph/adjacency_list.hpp>

using Graph = boost::adjacency_list<boost::listS, boost::vecS, boost::undirectedS>;
using namespace gmpp::algo;

namespace {

    struct Scn
    {
        Graph g;
        std::vector<std::size_t> separator;
    };

    struct Exp
    {
        std::vector<std::size_t> vertex_color_map;
        std::vector<std::set<std::size_t>> components;
    };

}

TEST_CASE("separation tests", "[ut][separate]")
{
    Scn scn;
    Exp exp;

    SECTION("empty graph")
    {
        exp.vertex_color_map.clear();
    }

    SECTION("size 1 graph")
    {
        add_vertex(scn.g);
        exp.components = {  {0} };
        exp.vertex_color_map.resize(1, 0);
    }

    SECTION("more complex graph")
    {
        scn.g = Graph(5);
        add_edge(0,1,scn.g);
        add_edge(0,3,scn.g);
        add_edge(1,2,scn.g);
        add_edge(1,3,scn.g);
        add_edge(1,4,scn.g);
        add_edge(2,3,scn.g);

        SECTION("empty separator") 
        {
            exp.vertex_color_map.resize(5, 0);
            exp.components = { {0,1,2,3,4}  };
        }

        SECTION("separator [02]")
        {
            scn.separator = {0,2};
            exp.vertex_color_map.resize(5, 0);
            exp.vertex_color_map[0] = Separator::separator_color();
            exp.vertex_color_map[2] = Separator::separator_color();
            exp.components = {{ 0, 1, 2, 3, 4  } };
        }

        SECTION("separator [13]")
        {
            scn.separator = {1,3};
            exp.vertex_color_map = {0, Separator::separator_color(), 1, Separator::separator_color(), 2};
            exp.components = {{0,1,3}, {1,2,3}, {1,4}};
        }

        SECTION("separator [013]")
        {
            scn.separator = {0,1,3};
            exp.vertex_color_map = {Separator::separator_color(), Separator::separator_color(), 0, Separator::separator_color(), 1};
            exp.components = { {1,2,3}, {1,4}};
        }
        SECTION("separator [1]")
        {
            scn.separator = {1};
            exp.vertex_color_map = {0, Separator::separator_color(), 0, 0, 1 };
            exp.components = { {0,1,2,3}, {1,4}};
        }

        
        SECTION("separator [0,1,2,3]")
        {
            scn.separator = {0,1,2,3};
            exp.vertex_color_map = {Separator::separator_color(), Separator::separator_color(), Separator::separator_color(), Separator::separator_color(), 0 };
            exp.components = { {1,4}};
        }

        SECTION("complete graph as separator")
        {
            scn.separator = {0,1,2,3,4};
            exp.vertex_color_map.resize(5, Separator::separator_color());
            exp.components = {};
        }
    }

    Separator result (gubg::make_range(scn.separator), scn.g);
    for(std::size_t i = 0; i < num_vertices(scn.g); ++i)
        REQUIRE(result.get_color(i) == exp.vertex_color_map[i]);
    
    REQUIRE(result.num_components() == exp.components.size());
    for(std::size_t c = 0; c < result.num_components(); ++c)
    {
        REQUIRE(result.component(c) == exp.components[c]);

    }
}
